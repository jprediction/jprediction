/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

import gui.MainGUI;

import javax.swing.*;

/**
 * jPrediction
 * 
 * @author Yury V. Reshetov
 * @version 9.00
 */
public class JPrediction {
	
	public static void main(String[] args) {
		// Use an appropriate Look and Feel
		try {
			String systemLookAndFeelClassName = UIManager
					.getSystemLookAndFeelClassName();
			// ������������� LookAndFeel ��������������� ������� ������������
			// �������
			UIManager.setLookAndFeel(systemLookAndFeelClassName);
		} catch (UnsupportedLookAndFeelException ex) {
			ex.printStackTrace();
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
		} catch (InstantiationException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		// Turn off metal's use of bold fonts
		UIManager.put("swing.boldMetal", Boolean.FALSE);

		// Schedule a job for the event dispatch thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new MainGUI().createGUI();
			}
		});
	}
}
