package gui.tables.rows;
/*
libVMR

Copyright (C) 2014,  Yury V. Reshetov

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

/**
 * ������ �������
 * @author Yury V. Reshetov
 * @version 6.00
 */
	public class TableRows {
		 
		/**
		 * �������� ����������
		 */
        private String name;
        /**
         * ���������� � ����������
         */
        private String description;
        /**
         * �������� ����������
         */
        private String value;
        
        /**
         * �����������
         * @param name �������� ����������
         * @param description ���������� ��� ����������
         * @param value �������� ����������
         */
        public TableRows(String name, String description, String value) {
            this.setName(name);
            this.setValue(value);
            this.setDescription(description);
        }
 
        /**
         * ���������� �������� ����������
         * @param name �������� ����������
         */
        public void setName(String name) {
            this.name = name;
        }
 
        /**
         * �������� �������� ����������
         * @return �������� ����������
         */
        public String getName() {
            return name;
        }
        
        /**
         * ���������� �������� ����������
         * @param size �������� ����������
         */
        public void setValue(String size) {
            this.value = size;
        }
        
        /**
         * �������� �������� ����������
         * @return �������� ����������
         */
        public String getValue() {
            return value;
        }
 
        /**
         * ���������� ���������� ��� ����������
         * @param description ����������
         */
        public void setDescription(String description) {
            this.description = description;
        }
        
        /**
         * �������� ���������� ��� ����������
         * @return ����������
         */
        public String getDescription() {
            return description;
        }
    }	
