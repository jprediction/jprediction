package gui.tables;




import gui.tables.rows.TableRows;

import java.util.*;

import javax.swing.event.*;
import javax.swing.table.*;

/**
 * ������ ������� �����������
 * @author Yury V. Reshetov
 * @version 7.00
 */

	public class PredictorsTableModel implements TableModel {
		 
		/**
		 * ������� ���������� �������
		 */
        private Set<TableModelListener> listeners = new HashSet<TableModelListener>();
        /**
         * ���������� ����� �������
         */
        private java.util.List<TableRows> rows;
        
        /**
         * �����������
         * @param tablerows ������ �������
         */
        public PredictorsTableModel(java.util.List<TableRows> tablerows) {
            this.rows = tablerows;
        }
 
    	@Override
        public void addTableModelListener(TableModelListener listener) {
            listeners.add(listener);
        }
 
    	@Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }
 
    	@Override
        public int getColumnCount() {
            return 3;
        }
 
    	@Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
            case 0:
                return "Name";
            case 1:
                return "Description";
            case 2:
                return "Value";
            }
            return "";
        }
 
    	@Override
        public int getRowCount() {
            return rows.size();
        }
 
    	@Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            TableRows row = rows.get(rowIndex);
            switch (columnIndex) {
            case 0:
                return row.getName();
            case 1:
                return row.getDescription();
            case 2:
                return row.getValue();
            }
            return "";
        }
 
    	@Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
    		return false;
        }
 
    	@Override
        public void removeTableModelListener(TableModelListener listener) {
            listeners.remove(listener);
        }
 
    	@Override
        public void setValueAt(Object value, int rowIndex, int columnIndex) {
        	TableRows bean = rows.get(rowIndex);
        	try {
        		Double.parseDouble(value.toString());
        	} catch (NumberFormatException nfe) {
            	bean.setValue("Error");
            	return;
        	}
        	bean.setValue(value.toString());
        }
 
    }
