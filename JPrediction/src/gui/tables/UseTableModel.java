package gui.tables;
/*
libVMR

Copyright (C) 2014,  Yury V. Reshetov

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/


import gui.tables.rows.TableRows;

import java.util.*;

import javax.swing.event.*;
import javax.swing.table.*;

/**
 * ������ �������
 * @author Yury V. Reshetov
 * @version 7.00
 */

	public class UseTableModel implements TableModel {
		 
		/**
		 * ������� ���������� �������
		 */
        private Set<TableModelListener> listeners = new HashSet<TableModelListener>();
        /**
         * ���������� ����� �������
         */
        private java.util.List<TableRows> rows;
        
        /**
         * �����������
         * @param tablerows ������ �������
         */
        public UseTableModel(java.util.List<TableRows> tablerows) {
            this.rows = tablerows;
        }
 
    	@Override
        public void addTableModelListener(TableModelListener listener) {
            listeners.add(listener);
        }
 
    	@Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }
 
    	@Override
        public int getColumnCount() {
            return 3;
        }
 
    	@Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
            case 0:
                return "Name";
            case 1:
                return "Description";
            case 2:
                return "Value";
            }
            return "";
        }
 
    	@Override
        public int getRowCount() {
            return rows.size();
        }
 
    	@Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            TableRows row = rows.get(rowIndex);
            switch (columnIndex) {
            case 0:
                return row.getName();
            case 1:
                return row.getDescription();
            case 2:
                return row.getValue();
            }
            return "";
        }
 
    	@Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
        	
            return (columnIndex == 2);
        }
 
    	@Override
        public void removeTableModelListener(TableModelListener listener) {
            listeners.remove(listener);
        }
 
    	@Override
        public void setValueAt(Object value, int rowIndex, int columnIndex) {
        	TableRows bean = rows.get(rowIndex);
        	try {
        		Double.parseDouble(value.toString());
        	} catch (NumberFormatException nfe) {
            	bean.setValue("Error");
            	return;
        	}
        	bean.setValue(value.toString());
        }
 
    }
