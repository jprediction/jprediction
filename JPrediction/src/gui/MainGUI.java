/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package gui;

import gui.graphics.*;
import gui.tables.*;
import gui.tables.rows.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import libvmr.classificators.ternary.*;
import libvmr.parsers.*;
import libvmr.selectors.*;

/**
 * ������� ���� jPrediction
 * 
 * @author Yury V. Reshetov
 * @version 9.00
 */

public class MainGUI {

	/**
	 * ������
	 */
	private static Parser parser = new CSVParser();

	/**
	 * ��� �����
	 */
	private static String filename = "";

	/**
	 * ������ ����� �������
	 */
	private static ArrayList<TableRows> tablerows = new ArrayList<TableRows>();
	/**
	 * ������ ��� �������
	 */
	private static TableModel tablemodel = new UseTableModel(tablerows);
	/**
	 * �������
	 */
	private static JTable maintable = new JTable(tablemodel);
	/**
	 * ������ �� ����������� ��� ����� ������
	 */
	private static JScrollPane usescrollPanel = new JScrollPane();
	/**
	 * ������ �� ����������� ��� ������� �����������
	 */
	private static JScrollPane predictorsscrollPanel = new JScrollPane();
	/**
	 * ������������� �������
	 */
	private static JLabel label = new JLabel("Hi!");

	/**
	 * ��������� �������������
	 */
	private static TernaryClassificator classificator = null;

	/**
	 * ����������� �������� ������������ (Java Swing GUI)
	 */
	public void createGUI() {

		JFrame frame = new JFrame("jPrediction");

		frame.setIconImage(new CreateIcon().createIcon());

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel mainpanel = new JPanel();
		mainpanel.setLayout(new BorderLayout());

		JPanel maineastpanel = new JPanel();
		mainpanel.add(maineastpanel, BorderLayout.EAST);
		JPanel mainwestpanel = new JPanel();
		mainpanel.add(mainwestpanel, BorderLayout.WEST);
		JPanel mainsouthpanel = new JPanel();
		mainpanel.add(mainsouthpanel, BorderLayout.SOUTH);
		JPanel mainnorthpanel = new JPanel();
		mainpanel.add(mainnorthpanel, BorderLayout.NORTH);
		JPanel maincenterpanel = new JPanel();
		mainpanel.add(maincenterpanel, BorderLayout.CENTER);

		CardLayout layout = new CardLayout();

		maincenterpanel.setLayout(layout);
		maincenterpanel.add(new JPanel(), "Begin");

		JPanel messagepanel = new JPanel();
		messagepanel.setLayout(new FlowLayout());
		messagepanel.add(label, "");
		maincenterpanel.add(messagepanel, "Please_wait");

		String[] columnNames = { "Name", "Description", "Value", };

		String[][] data = { 
				//{ "Bias", "Percents", "0" },
				{ "Sensitivity of generalization abiliy", "Percents", "0" },
				{ "Specificity of generalization ability", "Percents", "0" },
				{ "Generalization abiliy", "Percents", "0" },
				{ "True Positives", "Patterns", "0" },
				{ "False Positives", "Patterns", "0" },
				{ "True Negatives", "Patterns", "0" },
				{ "False Negatives", "Patterns", "0" },
				//{ "Discrepancies", "Patterns", "0" },
				{ "Total", "Patterns", "0" }, };

		JTable table = new JTable(data, columnNames);
		JScrollPane qualityscrollPanel = new JScrollPane(table);

		JPanel qualitypanel = new JPanel();
		qualitypanel.setLayout(new BorderLayout());

		JPanel qualityeastpanel = new JPanel();
		qualitypanel.add(qualityeastpanel, BorderLayout.EAST);
		JPanel qualitywestpanel = new JPanel();
		qualitypanel.add(qualitywestpanel, BorderLayout.WEST);
		JPanel qualitysouthpanel = new JPanel();
		qualitypanel.add(qualitysouthpanel, BorderLayout.SOUTH);
		JLabel qualitylabel = new JLabel("The quality of modeling:");
		qualitypanel.add(qualitylabel, BorderLayout.NORTH);
		qualitypanel.add(qualityscrollPanel, BorderLayout.CENTER);

		maincenterpanel.add(qualitypanel, "Quality");

		JPanel usepanel = new JPanel();
		usepanel.setLayout(new BorderLayout());

		JPanel useeastpanel = new JPanel();
		usepanel.add(useeastpanel, BorderLayout.EAST);
		JPanel usewestpanel = new JPanel();
		usepanel.add(usewestpanel, BorderLayout.WEST);
		JPanel usenorthpanel = new JPanel();
		usepanel.add(usenorthpanel, BorderLayout.NORTH);
		JLabel uselabel = new JLabel("Enter data and press OK");
		JPanel usesouthpanel = new JPanel();
		usesouthpanel.setLayout(new FlowLayout());
		usesouthpanel.add(uselabel);
		usepanel.add(usesouthpanel, BorderLayout.SOUTH);
		usepanel.add(usescrollPanel, BorderLayout.CENTER);

		maincenterpanel.add(usepanel, "Use");

		JPanel predictorspanel = new JPanel();
		predictorspanel.setLayout(new BorderLayout());

		JPanel predictorseastpanel = new JPanel();
		predictorspanel.add(predictorseastpanel, BorderLayout.EAST);
		JPanel predictorswestpanel = new JPanel();
		predictorspanel.add(predictorswestpanel, BorderLayout.WEST);
		// JPanel predictorsnorthpanel = new JPanel();
		JLabel predictorslabel = new JLabel();
		predictorspanel.add(predictorslabel, BorderLayout.NORTH);
		JPanel predictorssouthpanel = new JPanel();
		// predictorsnorthpanel.setLayout(new FlowLayout());
		// predictorsnorthpanel.add(predictorslabel);
		predictorspanel.add(predictorssouthpanel, BorderLayout.SOUTH);
		predictorspanel.add(predictorsscrollPanel, BorderLayout.CENTER);

		maincenterpanel.add(predictorspanel, "predictors");

		JButton ok = new JButton("OK");
		ok.setActionCommand("ok");
		ok.setEnabled(false);
		ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				uselabel.setText("Enter data and press OK");
				ok.setEnabled(false);
				String[] ids = classificator.getIds();
				String[] data = new String[ids.length - 1];
				double[] pattern = new double[data.length];
				boolean error = false;
				String s_error = "";
				int i = 0;
				try {
					for (i = 0; i < data.length; i++) {
						pattern[i] = Double.parseDouble(tablerows.get(i)
								.getValue());
					}
				} catch (NumberFormatException nfe) {
					nfe.printStackTrace();
					error = true;
					s_error = "Erroneous data for predictor \"" + ids[i]
							+ "\": " + tablerows.get(i).getValue();
				}
				if (error) {
					uselabel.setText(s_error);
				} else {
					String decision = classificator.getResult(pattern);// "-";
					uselabel.setText("Result = " + decision);
				}
			}
		});
		mainsouthpanel.add(ok);

		JMenuBar menubar = new JMenuBar();
		JMenu filemenu = new JMenu("File");
		JMenu viewmenu = new JMenu("View");
		viewmenu.setEnabled(false);
		JMenuItem createmenuitem = new JMenuItem("Create model ...");
		// set shortcut F8
		KeyStroke f8 = KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0);
		// set the accelerator
		createmenuitem.setAccelerator(f8);
		createmenuitem.setActionCommand("open");

		JMenuItem loadmenuitem = new JMenuItem("Load model ...");
		// set shortcut F3
		KeyStroke f3 = KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0);
		// set the accelerator
		loadmenuitem.setAccelerator(f3);
		loadmenuitem.setActionCommand("load");
		JMenuItem savemenuitem = new JMenuItem("Save model ...");
		// set shortcut F2
		KeyStroke f2 = KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0);
		// set the accelerator
		savemenuitem.setAccelerator(f2);

		JMenuItem usemenuitem = new JMenuItem("Use model");
		// set shortcut F4
		KeyStroke f4 = KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0);
		// set the accelerator
		usemenuitem.setAccelerator(f4);

		JMenuItem predictorsmenuitem = new JMenuItem(
				"View a significant of predictors");
		// set shortcut F5
		KeyStroke f5 = KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0);
		// set the accelerator
		predictorsmenuitem.setAccelerator(f5);

		JMenuItem qualitymenuitem = new JMenuItem("View a quality of modeling");
		// set shortcut F5
		KeyStroke f6 = KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0);
		// set the accelerator
		qualitymenuitem.setAccelerator(f6);

		loadmenuitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				FileDialog fd = new FileDialog(frame, "Load VMR file",
						FileDialog.LOAD);
				fd.setFile("*.vmr");
				fd.setVisible(true);
				if (fd.getFile() != null) {
					usemenuitem.setEnabled(false);
					savemenuitem.setEnabled(false);
					createmenuitem.setEnabled(false);
					loadmenuitem.setEnabled(false);
					qualitymenuitem.setEnabled(false);
					predictorsmenuitem.setEnabled(false);
					ok.setEnabled(false);

					filename = fd.getFile();
					int len = filename.length() - 4;
					filename = filename.substring(0, len);
					frame.setTitle(filename.trim() + " | jPrediction");
					File file = new File(fd.getDirectory() + fd.getFile());
					try {
						ObjectInputStream ois = new ObjectInputStream(
								new FileInputStream(file));
						classificator = (TernaryClassificator) ois.readObject();
						ois.close();
						label.setText("\"" + filename + "\" model loaded");
						layout.show(maincenterpanel, "Please_wait");
						viewmenu.setEnabled(true);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
					usemenuitem.setEnabled(true);
					savemenuitem.setEnabled(false);
					createmenuitem.setEnabled(true);
					loadmenuitem.setEnabled(true);
					qualitymenuitem.setEnabled(true);
					predictorsmenuitem.setEnabled(true);
				}
			}
		});
		createmenuitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				FileDialog fd = new FileDialog(frame, "Load CSV file",
						FileDialog.LOAD);
				fd.setFile("*.csv");
				fd.setVisible(true);
				if (fd.getFile() != null) {
					usemenuitem.setEnabled(false);
					savemenuitem.setEnabled(false);
					createmenuitem.setEnabled(false);
					loadmenuitem.setEnabled(false);
					qualitymenuitem.setEnabled(false);
					predictorsmenuitem.setEnabled(false);
					ok.setEnabled(false);

					parser = new CSVParser();
					filename = fd.getFile();
					int len = filename.length() - 4;
					filename = filename.substring(0, len);
					frame.setTitle(filename.trim() + " | jPrediction");

					label.setText("Please wait");
					layout.show(maincenterpanel, "Please_wait");
					messagepanel.paintImmediately(messagepanel.getBounds());
					maincenterpanel.paintImmediately(maincenterpanel
							.getBounds());

					try {
						Thread.sleep(1000);
					} catch (InterruptedException ie) {

					}
					// ������� �����
					File file = new File(fd.getDirectory() + fd.getFile());
					double[][] samples = parser.parsing(file);
					if (parser.getError() != null) {
						label.setText(parser.getError());
						layout.show(maincenterpanel, "Please_wait");
						createmenuitem.setEnabled(true);
						return;
					}

					// ����� ������ ������ �� ���������� �����������
					classificator = new SimpleSelector().getBestModel(samples, parser);

					// ����� ������������� ��������� ������
					if (classificator.getGa() > 0d) {
						//int discrepancies = classificator.getDiscrepancies();
						int tp = classificator.getTp();
						int fp = classificator.getFp();
						int tn = classificator.getTn();
						int fn = classificator.getFn();
						double ga = classificator.getGa();
						double sensitivity = classificator.getSensitivity();
						double specificity = classificator.getSpecificity();
						//double bias = classificator.getBias();

						//data[0][2] = bias + "%";
						data[0][2] = sensitivity + "%";
						data[1][2] = specificity + "%";
						data[2][2] = ga + "%";
						data[3][2] = tp + "";
						data[4][2] = fp + "";
						data[5][2] = tn + "";
						data[6][2] = fn + "";
						//data[8][2] = discrepancies + "";

						int total = classificator.getTotal();
						data[7][2] = total + "";

						layout.show(maincenterpanel, "Quality");

						savemenuitem.setEnabled(true);
						usemenuitem.setEnabled(true);
						viewmenu.setEnabled(true);
						qualitymenuitem.setEnabled(true);
						predictorsmenuitem.setEnabled(true);
						createmenuitem.setEnabled(true);
						loadmenuitem.setEnabled(true);
					} else {
						viewmenu.setEnabled(false);
						createmenuitem.setEnabled(true);
						savemenuitem.setEnabled(false);
						usemenuitem.setEnabled(false);
						predictorsmenuitem.setEnabled(false);
						qualitymenuitem.setEnabled(false);
						label.setText("Garbage In, Garbage Out");
						layout.show(maincenterpanel, "Please_wait");
					}
				}
			}
		});
		savemenuitem.setActionCommand("save");
		savemenuitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				FileDialog fd = new FileDialog(frame, "Save Model",
						FileDialog.SAVE);
				fd.setFile(filename + ".vmr");
				fd.setVisible(true);
				if (fd.getFile() != null) {
					usemenuitem.setEnabled(false);
					savemenuitem.setEnabled(false);
					createmenuitem.setEnabled(false);
					loadmenuitem.setEnabled(false);
					predictorsmenuitem.setEnabled(false);
					qualitymenuitem.setEnabled(false);
					ok.setEnabled(false);

					String filename = fd.getFile();
					if (!filename.endsWith(".vmr")) {
						filename = filename + ".vmr";
					}
					try {
						File file = new File(fd.getDirectory() + filename);
						ObjectOutputStream oos = new ObjectOutputStream(
								new FileOutputStream(file));
						oos.writeObject(classificator);
						oos.flush();
						oos.close();
						label.setText("Model \"" + filename
								+ "\" successfully saved");
						layout.show(maincenterpanel, "Please_wait");

						int len = filename.length() - 4;
						filename = filename.substring(0, len);

						File file1 = new File(fd.getDirectory() + filename
								+ ".java");
						try {

							PrintWriter pw = new PrintWriter(new FileWriter(
									file1));
							String formula = "";

							formula += classificator.getSecondBinaryClassificator().getJavaCode()
									+ "\n"
									+ classificator.getFirstBinaryClassificator().getJavaCode()
									+ "\n";
							String[] ids = classificator.getIds();
							int length = ids.length - 1;

							formula += "/**\n";
							//formula += "* Bias: " + classificator.getBias() + "%\n";
							formula += "* Sensitivity of generalization abiliy: "
									+ classificator.getSensitivity() + "%\n";
							formula += "* Specificity of generalization ability: "
									+ classificator.getSpecificity() + "%\n";
							formula += "* Generalization ability: "
									+ classificator.getGa() + "%\n";
							formula += "* TruePositives: " + classificator.getTp()
									+ "\n";
							formula += "* FalsePositives: " + classificator.getFp()
									+ "\n";
							formula += "* TrueNegatives: " + classificator.getTn()
									+ "\n";
							formula += "* FalseNegatives: " + classificator.getFn()
									+ "\n";
							//formula += "* Discrepancies: " + classificator.getDiscrepancies() + "\n";
							formula += "* Total patterns in out of samples with statistics: "
									+ classificator.getTotal() + "\n";
							formula += "*/\n";

							for (int i = 0; i < length; i++) {
								formula += "//Variable x" + i + ": " + ids[i]
										+ "\n";
							}

							formula += "int getTernaryClassificator(";
							for (int i = 0; i < length; i++) {
								if (i == 0) {
									formula += "double x" + i;
								} else {
									formula += ", double x" + i;
								}
							}
							formula += ") {\n";
							formula += "   double result1 = getBinaryClassificator1(";
							for (int i = 0; i < length; i++) {
								if (i == 0) {
									formula += "x" + i;
								} else {
									formula += ", x" + i;
								}
							}
							formula += ");\n";
							formula += "   double result2 = getBinaryClassificator2(";
							for (int i = 0; i < length; i++) {
								if (i == 0) {
									formula += "x" + i;
								} else {
									formula += ", x" + i;
								}
							}

							formula += ");\n";
							formula += "   if ((result1 * result2) > 0.0) {\n";
							formula += "      if (result1 > 0.0) {\n";
							formula += "         return(1);\n";
							formula += "      } else {\n";
							formula += "         return(0);\n";
							formula += "      }\n";
							formula += "   }\n";
							formula += "   return(-1);\n";
							formula += "}\n";
							StringTokenizer st = new StringTokenizer(formula,
									"\n");
							while (st.hasMoreTokens()) {
								pw.println(st.nextToken());
							}
							pw.flush();
							pw.close();
						} catch (Exception ex) {
							ex.printStackTrace();
						}

					} catch (Exception ex) {
						ex.printStackTrace();
					}

					usemenuitem.setEnabled(true);
					createmenuitem.setEnabled(true);
					loadmenuitem.setEnabled(true);
					savemenuitem.setEnabled(true);
					predictorsmenuitem.setEnabled(true);
					qualitymenuitem.setEnabled(true);
				}
			}
		});
		savemenuitem.setEnabled(false);

		usemenuitem.setActionCommand("use");
		usemenuitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				uselabel.setText("Enter data and press OK");
				tablerows = new ArrayList<TableRows>();

				String[] ids = classificator.getIds();
				String[] units = classificator.getUnits();
				for (int currentid = 0; currentid < (ids.length - 1); currentid++) {
					tablerows.add(new TableRows(ids[currentid] + "",
							units[currentid] + "", "0"));
				}
				usepanel.remove(usescrollPanel);
				tablemodel = new UseTableModel(tablerows);
				maintable = new JTable(tablemodel);
				usescrollPanel = new JScrollPane(maintable);
				usepanel.add(usescrollPanel, BorderLayout.CENTER);

				layout.show(maincenterpanel, "Use");
				ok.setEnabled(true);
				frame.pack();
			}
		});
		usemenuitem.setEnabled(false);

		predictorsmenuitem.setActionCommand("predictors");
		predictorsmenuitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				predictorslabel.setText("Significance of the predictors:");
				tablerows = new ArrayList<TableRows>();

				String[] ids = classificator.getIds();
				int[] powerpredictors = classificator.getPredictorsPower();

				int min = powerpredictors[0];
				for (int i = 1; i < powerpredictors.length; i++) {
					if (powerpredictors[i] < min) {
						min = powerpredictors[i];
					}
				}

				int max = powerpredictors[0];
				for (int i = 1; i < powerpredictors.length; i++) {
					if (powerpredictors[i] > max) {
						max = powerpredictors[i];
					}
				}

				for (int currentid = 0; currentid < (ids.length - 1); currentid++) {
					String description = "-";
					if (powerpredictors[currentid] == min) {
						description = "The worst predictor";
					}
					if (powerpredictors[currentid] == max) {
						description = "The best predictor";
					}
					tablerows.add(new TableRows(ids[currentid] + "",
							description, powerpredictors[currentid] + ""));
				}
				predictorspanel.remove(predictorsscrollPanel);
				tablemodel = new PredictorsTableModel(tablerows);
				maintable = new JTable(tablemodel);
				predictorsscrollPanel = new JScrollPane(maintable);
				predictorspanel.add(predictorsscrollPanel, BorderLayout.CENTER);

				layout.show(maincenterpanel, "predictors");
				frame.pack();
			}
		});

		predictorsmenuitem.setEnabled(false);

		qualitymenuitem.setActionCommand("quality");
		qualitymenuitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//int discrepancies = classificator.getDiscrepancies();
				int tp = classificator.getTp();
				int fp = classificator.getFp();
				int tn = classificator.getTn();
				int fn = classificator.getFn();
				double ga = classificator.getGa();
				double sensitivity = classificator.getSensitivity();
				double specificity = classificator.getSpecificity();
				//double bias = classificator.getBias();

				//data[0][2] = bias + "%";
				data[0][2] = sensitivity + "%";
				data[1][2] = specificity + "%";
				data[2][2] = ga + "%";
				data[3][2] = tp + "";
				data[4][2] = fp + "";
				data[5][2] = tn + "";
				data[6][2] = fn + "";
				//data[7][2] = discrepancies + "";

				int total = classificator.getTotal();
				data[7][2] = total + "";

				layout.show(maincenterpanel, "Quality");
			}
		});
		qualitymenuitem.setEnabled(false);

		JMenuItem exitmenuitem = new JMenuItem("Exit");
		// set shortcut F12
		KeyStroke f12 = KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0);
		// set the accelerator
		exitmenuitem.setAccelerator(f12);
		exitmenuitem.setActionCommand("exit");
		exitmenuitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		filemenu.add(createmenuitem);
		filemenu.addSeparator();
		filemenu.add(loadmenuitem);
		filemenu.add(savemenuitem);
		filemenu.addSeparator();
		filemenu.add(usemenuitem);
		filemenu.addSeparator();
		filemenu.add(exitmenuitem);

		viewmenu.add(predictorsmenuitem);
		viewmenu.add(qualitymenuitem);

		JMenu about = new JMenu("About ...");
		JMenuItem version = new JMenuItem("jPrediction. Version: 10.00 Release");
		JMenuItem copyright = new JMenuItem(
				"CopyRight (�) 2014-2016, Yury V. Reshetov");
		JMenuItem licencse = new JMenuItem("General Public License version 3");
		JMenuItem siteurl = new JMenuItem("Help online ...");
		// set shortcut F1
		KeyStroke f1 = KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0);
		// set the accelerator
		siteurl.setAccelerator(f1);
		siteurl.setActionCommand("link");
		siteurl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop d = Desktop.getDesktop();

					d.browse(new URI(String.format(
							"http://jprediction.com/en/node/27", URLEncoder
									.encode("������ � ����� ��������", "UTF8"))));
				} catch (IOException ioe) {
					ioe.printStackTrace();
				} catch (URISyntaxException use) {
					use.printStackTrace();
				}
			}
		});
		JMenuItem opensource = new JMenuItem("Get the open source code ...");
		opensource.setActionCommand("opensource");
		opensource.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop d = Desktop.getDesktop();

					d.browse(new URI(
							String.format(
									"https://bitbucket.org/jprediction/jprediction/downloads",
									URLEncoder.encode(
											"������ � ����� ��������", "UTF8"))));
				} catch (IOException ioe) {
					ioe.printStackTrace();
				} catch (URISyntaxException use) {
					use.printStackTrace();
				}
			}
		});
		about.add(version);
		about.add(copyright);
		about.add(licencse);
		about.add(siteurl);
		about.add(opensource);
		menubar.add(filemenu);
		menubar.add(viewmenu);
		menubar.add(about);
		frame.setJMenuBar(menubar);
		frame.getContentPane().add(mainpanel);
		frame.setPreferredSize(new Dimension(500, 300));
		frame.pack();
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
	}
}