/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package libvmr.paralleling;

import libvmr.classificators.neuronets.*;

/**
 * ����������������� ��������� ����������
 * 
 * @author Yury V. Reshetov
 * @version 10.00
 */
public class SeparateParallelingTrain implements ParallelingTrain {
	/**
	 * ��������� ������
	 */
	private NeuroNet neuronet = null;

	double[][] train = null;
	double[][] test = null;

	/**
	 * ���� ���������� ������
	 */
	private boolean ready = false;


	/**
	 * �����������
	 * 
	 * @param neuronet
	 *            ��������� ��������� ������
	 * @param separator
	 *            ���������
	 */
	public SeparateParallelingTrain(NeuroNet neuronet, double[][] train,
			double[][] test) {
		this.train = train;
		this.test = test;
		this.neuronet = neuronet;
		Thread th = new Thread(this);
		th.start();
	}


	@Override
	public void run() {
		this.neuronet.train(this.train, this.test);
		ready = true;
	}

	/**
	 * ��������� ���� ���������� ������
	 * 
	 * @return the ��������� �����
	 */
	@Override
	public boolean isReady() {
		return ready;
	}
}
