/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package libvmr.selectors;

import java.util.*;

import libvmr.classificators.neuronets.*;
import libvmr.classificators.ternary.*;
import libvmr.parsers.Parser;
import libvmr.separators.*;

/**
 * ����� ���������� ���������� �������������� �� ���������� �����������
 * 
 * @author Yury V. Reshetov
 * @version 10.00
 */
public class SimpleSelector implements Selector {
	

	@Override
	public TernaryClassificator getBestModel(double[][] samples, Parser parser) {

		TernaryClassificator result = null;

		int kernels = Runtime.getRuntime().availableProcessors();

		ArrayList<TernaryClassificator> containers = new ArrayList<TernaryClassificator>();

		int[] predictors = null;
		Separator separator = new Separator(samples);

		int[] bestpredictors = null;
		boolean readyloop = false;
		for (int maxpredictors = 4; ((maxpredictors < Math.min(
				separator.getPredictorsCount(), 10)) && (!readyloop)); maxpredictors++) {
			readyloop = true;
			predictors = new int[maxpredictors];
			if (maxpredictors == 4) {
				for (int pr = 0; pr < maxpredictors; pr++) {
					predictors[pr] = pr;
				}
				bestpredictors = new int[maxpredictors];
				for (int i = 0; i < maxpredictors; i++) {
					bestpredictors[i] = predictors[i];
				}
			} else {
				for (int pr = 0; pr < maxpredictors; pr++) {
					predictors[pr] = pr;
				}
			}
			// ������ ��������� ��� ������ �� ������
			int worstpredictor = maxpredictors - 1;
			TernaryClassificator bestclassificator = null;
			for (int nextpredictor = (maxpredictors - 1); nextpredictor < separator
					.getPredictorsCount(); nextpredictor++) {

				/*
				if (bestclassificator != null) {
					int[] predictorspower = bestclassificator.getPredictorsPower();
					worstpredictor = Randomizator.nextInt(predictorspower.length);					
				}
				*/
				predictors[worstpredictor] = nextpredictor;

				for (int i = 0; i < kernels; i++) {
					separator = new Separator(samples);
					result = new VMRTernaryClassificator(
							new VMRNeuroNet(parser),
							new VMRNeuroNet(parser),
							new GMDHNeuroNet(parser),
							parser.getIDs(predictors),
							parser.getUnits(predictors));
					result.train(separator, predictors);
					containers.add(result);
				}
				boolean ready = false;
				do {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException ie) {
						ie.printStackTrace();
					}
					ready = true;
					for (int i = 0; i < containers.size(); i++) {
						if (!containers.get(i).isReady()) {
							ready = false;
						}
					}
				} while (!ready);

				result = containers.get(0);
				double minga = result.getMinGa();

				for (int i = 1; i < containers.size(); i++) {
					if (containers.get(i).getMinGa() > minga) {
						result = containers.get(i);
						minga = result.getMinGa();
					}
				}

				if (bestclassificator == null) {
					bestclassificator = result;
				} else {
					if (result.getMinGa() > bestclassificator.getMinGa()) {
						bestclassificator = result;
						bestpredictors = predictors.clone();
						readyloop = false;
					}
				}

				// ����� ���������� ���������� 
				
				int[] predictorspower = bestclassificator.getPredictorsPower();
				
				
				int min = predictorspower[0];
				worstpredictor = 0;
				for (int i = 1; i < predictorspower.length; i++) {
					if (Math.abs(predictorspower[i]) < min) {
						min = Math.abs(predictorspower[i]);
						worstpredictor = i;
					}
				}
				
				//worstpredictor = this.rand.nextInt(predictorspower.length);

				/*
				 * for (int i = 0; i < bestpredictors.length; i++) {
				 * System.out.println("Predictor[" + bestpredictors[i] + "]"); }
				 * 
				 * System.out.println(bestclassificator.getRight() + ", " +
				 * bestclassificator.getGa() + ", " +
				 * bestclassificator.getMinGa());
				 * System.out.println("Maxpredictors = " + maxpredictors);
				 */

				result = bestclassificator;
			}
		}
		return result;
	}

}
