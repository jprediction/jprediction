/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package libvmr.separators;

import java.io.*;
import java.util.*;

import libvmr.tools.*;

/**
 * ���������, ����������� ������� �� ��������� � �������� �����
 * 
 * @author Yury V. Reshetov
 * @version 10.00
 */
public class Separator implements Serializable {

	private static final long serialVersionUID = 1000L;
	/**
	 * �������� �������
	 */
	private double[][] samples = null;

	/**
	 * ������ ����� ��������� �������
	 */
	private double[][] trainsamples1 = null;
	/**
	 * ������ ����� ��������� �������
	 */
	private double[][] trainsamples2 = null;
	/**
	 * ������ ����� �������� �������
	 */
	private double[][] testsamples1 = null;
	/**
	 * ������ ����� �������� �������
	 */
	private double[][] testsamples2 = null;

	/**
	 * �����������
	 * 
	 * @param �����
	 *            ������� � ���������
	 */
	public Separator(double[][] patterns) {

		this.samples = Randomizator.shuffleArray(patterns);

		ArrayList<double[]> trues = new ArrayList<double[]>();
		ArrayList<double[]> falses = new ArrayList<double[]>();
		for (int i = 0; i < samples.length; i++) {
			if (samples[i][samples[i].length - 1] > 0.5) {
				trues.add(samples[i]);
			} else {
				falses.add(samples[i]);
			}
		}
		/*
		Collections.shuffle(trues);
		Collections.shuffle(falses);
		*/

		int lenmin = Math.min(trues.size(), falses.size());
		int lenmax = Math.max(trues.size(), falses.size());

		ArrayList<double[]> min = null;
		ArrayList<double[]> max = null;

		if (trues.size() == lenmin) {
			min = trues;
			max = falses;
		} else {
			min = falses;
			max = trues;
		}

		ArrayList<double[]> train1 = new ArrayList<double[]>();
		ArrayList<double[]> train2 = new ArrayList<double[]>();
		ArrayList<double[]> test1 = new ArrayList<double[]>();
		ArrayList<double[]> test2 = new ArrayList<double[]>();

		for (int i = 0; i < lenmin; i++) {
			double[] pattern = min.get(i);
			if ((i % 2) == 0) {
				train1.add(pattern);
				test2.add(pattern);
			} else {
				train2.add(pattern);
				test1.add(pattern);
			}
		}

		for (int i = 0; i < lenmin; i++) {
			double[] pattern = max.get(i);
			if ((i % 2) == 0) {
				train1.add(pattern);
				test2.add(pattern);
			} else {
				train2.add(pattern);
				test1.add(pattern);
			}
		}

		for (int i = lenmin; i < lenmax; i++) {
			double[] pattern = max.get(i);
			test1.add(pattern);
			test2.add(pattern);
		}

		this.trainsamples1 = new double[train1.size()][patterns[0].length];

		for (int i = 0; i < train1.size(); i++) {
			double[] pattern = train1.get(i);
			trainsamples1[i] = pattern;
		}

		this.trainsamples2 = new double[train2.size()][patterns[0].length];

		for (int i = 0; i < train2.size(); i++) {
			double[] pattern = train2.get(i);
			trainsamples2[i] = pattern;
		}

		this.testsamples1 = new double[test1.size()][patterns[0].length];

		for (int i = 0; i < test1.size(); i++) {
			double[] pattern = test1.get(i);
			testsamples1[i] = pattern;
		}

		this.testsamples2 = new double[test2.size()][patterns[0].length];

		for (int i = 0; i < test2.size(); i++) {
			double[] pattern = test2.get(i);
			testsamples2[i] = pattern;
		}

	}

	/**
	 * �������� ������ �������� ����� �������
	 * 
	 * @return ������ �������� ����� �������
	 */
	public double[][] getTestSamples1() {
		return this.testsamples1;
	}

	/**
	 * �������� ������ �������� ����� ������� �� �������� �����������
	 * 
	 * @return ������ �������� ����� �������
	 */
	public double[][] getTestSamples1(int[] predictors) {
		double[][] temp = this.rotator(this.testsamples1);
		double[][] result = new double[predictors.length + 1][this.testsamples1.length];
		for (int i = 0; i < predictors.length; i++) {
			result[i] = temp[predictors[i]];
		}
		result[predictors.length] = temp[temp.length - 1];
		return this.rotator(result);
	}

	/**
	 * �������� ������ �������� ����� �������
	 * 
	 * @return ������ �������� ����� �������
	 */
	public double[][] getTestSamples2() {
		return this.testsamples2;
	}

	/**
	 * �������� ������ �������� ����� ������� �� �������� �����������
	 * 
	 * @return ������ �������� ����� �������
	 */
	public double[][] getTestSamples2(int[] predictors) {
		double[][] temp = this.rotator(this.testsamples2);
		double[][] result = new double[predictors.length + 1][this.testsamples2.length];
		for (int i = 0; i < predictors.length; i++) {
			result[i] = temp[predictors[i]];
		}
		result[predictors.length] = temp[temp.length - 1];
		return this.rotator(result);
	}

	/**
	 * �������� ������ ��������� ����� �������
	 * 
	 * @return ��������� ����� �������
	 */
	public double[][] getTrainSamples1() {
		return this.trainsamples1;
	}

	/**
	 * �������� ������ ��������� ����� ������� �� �������� �����������
	 * 
	 * @return ������ ��������� �����
	 */
	public double[][] getTrainSamples1(int[] predictors) {
		double[][] temp = this.rotator(this.trainsamples1);
		double[][] result = new double[predictors.length + 1][this.trainsamples1.length];
		for (int i = 0; i < predictors.length; i++) {
			result[i] = temp[predictors[i]];
		}
		result[predictors.length] = temp[temp.length - 1];
		return this.rotator(result);
	}

	/**
	 * �������� ������ ��������� ����� �������
	 * 
	 * @return ��������� ����� �������
	 */
	public double[][] getTrainSamples2() {
		return this.trainsamples2;
	}

	/**
	 * �������� ������ ��������� ����� ������� �� �������� �����������
	 * 
	 * @return ������ ��������� �����
	 */
	public double[][] getTrainSamples2(int[] predictors) {
		double[][] temp = this.rotator(this.trainsamples2);
		double[][] result = new double[predictors.length + 1][this.trainsamples2.length];
		for (int i = 0; i < predictors.length; i++) {
			result[i] = temp[predictors[i]];
		}
		result[predictors.length] = temp[temp.length - 1];
		return this.rotator(result);
	}

	/**
	 * ��������� ����������� �������
	 * 
	 * @return ����������� �������
	 */
	public double[][] getSamples() {
		return samples;
	}

	/**
	 * ������������ ��������� ������ �� 90 ��������
	 * 
	 * @param array
	 *            �������������� ������
	 * @return ��������� ������
	 */
	private double[][] rotator(double[][] array) {
		double[][] result = new double[array[0].length][array.length];
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[0].length; j++) {
				result[j][i] = array[i][j];
			}
		}
		return result;
	}

	/**
	 * ������ ���������� ����������� � �������
	 * 
	 * @return ���������� ����������� � �������
	 */
	public int getPredictorsCount() {
		return (this.samples[0].length - 1);
	}
}
