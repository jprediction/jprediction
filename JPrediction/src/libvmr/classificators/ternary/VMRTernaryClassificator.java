/*
    libvmr
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package libvmr.classificators.ternary;

import libvmr.classificators.neuronets.*;
import libvmr.paralleling.*;
import libvmr.separators.*;

/**
 * ��������� ��� ����������
 * 
 * @author Yury V. Reshetov
 * @version 10.00
 */

public class VMRTernaryClassificator extends Thread implements
		TernaryClassificator {

	/**
	 * ����� ������
	 */
	private static final long serialVersionUID = 1000L;

	/**
	 * ��������� ����
	 */
	private NeuroNet nn1 = null;

	private NeuroNet nn2 = null;
	
	private NeuroNet nn3 = null;

	/**
	 * �������� �����������
	 */
	private String[] ids = null;

	/**
	 * ���������� ��� �����������
	 */
	private String[] units = null;

	private boolean ready = false;

	/**
	 * ���������� �����������
	 */
	private int discrepancies = 0;

	/**
	 * ���������� ��������� ���������������� ��������
	 */
	private int right = 0;
	/**
	 * ���������� ������� ���������������� ��������
	 */
	private int left = 0;
	/**
	 * ���������� ������� ������������� ��������
	 */
	private int tp = 0;
	/**
	 * ���������� ������������������ ��������
	 */
	private int fp = 0;
	/**
	 * ���������� ������� ������������� ��������
	 */
	private int tn = 0;
	/**
	 * ���������� ����� ������������� ��������
	 */
	private int fn = 0;
	/**
	 * ���������� �����������
	 */
	private double ga = 0d;

	/**
	 * ����������� ���������� �����������
	 */
	private double minga = 0d;

	/**
	 * ����������������
	 */
	private double sensitivity = 0d;

	/**
	 * �������������
	 */
	private double specificity = 0d;

	/**
	 * ������������
	 */
	private double bias = 0d;

	/**
	 * ����� �������� � �������� �������
	 */
	private int total = 0;

	private int[] predictors = null;

	/**
	 * ���������
	 */
	Separator separator = null;

	/**
	 * �����������
	 * 
	 * @param nn1
	 *            ������ ��������� ���� ��� �������������
	 * @param nn2
	 *            ������ ��������� ���� ��� �������������
	 * @param nn3
	 *            ������  ��������� ���� ��� ���������� ���������� �����������
	 * @param ids
	 *            �������� �����������
	 * @param units
	 *            ���������� � �����������
	 */
	public VMRTernaryClassificator(NeuroNet nn1, NeuroNet nn2, NeuroNet nn3, String[] ids,
			String[] units) {
		this.nn1 = nn1;
		this.nn2 = nn2;
		this.ids = ids;
		this.units = units;
		this.nn3 = nn3;
	}

	@Override
	public String getResult(double[] pattern) {
		double result1 = this.nn1.getDecision(pattern);
		double result2 = this.nn2.getDecision(pattern);
		String decision = "-";
		if ((result1 * result2) > 0d) {
			decision = "0";
			if (result1 > 0d) {
				decision = "1";
			}
		}
		return decision;
	}

	@Override
	public NeuroNet getFirstBinaryClassificator() {
		return this.nn1;
	}

	@Override
	public NeuroNet getSecondBinaryClassificator() {
		return this.nn2;
	}

	@Override
	public String[] getIds() {
		return this.ids;
	}

	@Override
	public String[] getUnits() {
		return this.units;
	}

	@Override
	public void train(Separator separator) {
		this.separator = separator;
		this.ready = false;
		this.start();
	}

	@Override
	public void train(Separator separator, int[] predictors) {
		this.predictors = predictors;
		this.separator = separator;
		this.ready = false;
		this.start();
	}

	@Override
	public boolean isReady() {
		return this.ready;
	}

	@Override
	public int getDiscrepancies() {
		return this.discrepancies;
	}

	@Override
	public int getRight() {
		return this.right;
	}

	@Override
	public int getLeft() {
		return this.left;
	}

	@Override
	public int getTp() {
		return this.tp;
	}

	@Override
	public int getFp() {
		return this.fp;
	}

	@Override
	public int getTn() {
		return this.tn;
	}

	@Override
	public int getFn() {
		return this.fn;
	}

	@Override
	public double getGa() {
		return this.ga;
	}

	@Override
	public double getMinGa() {
		return this.minga;
	}

	@Override
	public double getSensitivity() {
		return this.sensitivity;
	}

	@Override
	public double getSpecificity() {
		return this.specificity;
	}

	@Override
	public double getBias() {
		return this.bias;
	}

	@Override
	public void run() {
		ParallelingTrain pt1 = null;
		if (this.predictors == null) {
			pt1 = new SeparateParallelingTrain(this.nn1,
					this.separator.getTrainSamples1(),
					this.separator.getTestSamples1());
		} else {
			pt1 = new SeparateParallelingTrain(this.nn1,
					this.separator.getTrainSamples1(predictors),
					this.separator.getTestSamples1(predictors));
		}
		ParallelingTrain pt2 = null;
		if (this.predictors == null) {
			pt2 = new SeparateParallelingTrain(this.nn2,
					this.separator.getTrainSamples2(),
					this.separator.getTestSamples2());
		} else {
			pt2 = new SeparateParallelingTrain(this.nn2,
					this.separator.getTrainSamples2(predictors),
					this.separator.getTestSamples2(predictors));
		}
		ParallelingTrain pt3 = null;
		if (this.predictors == null) {
			pt3 = new SeparateParallelingTrain(this.nn3,
					this.separator.getTrainSamples2(),
					this.separator.getTestSamples2());
		} else {
			pt3 = new SeparateParallelingTrain(this.nn3,
					this.separator.getTrainSamples2(predictors),
					this.separator.getTestSamples2(predictors));
		}
		while (!(pt1.isReady() && pt2.isReady() && pt3.isReady())) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
		}
		if (((this.nn1.getGeneralizationAbility() > 0d) && (this.nn2
				.getGeneralizationAbility() > 0d)) || (this.predictors != null)) {
			this.right = this.nn1.getTruePositives() + this.nn1.getTrueNegatives() + this.nn2.getTruePositives() + this.nn2.getTrueNegatives();
			this.left = this.nn1.getFalsePositives() + this.nn1.getFalseNegatives() + this.nn2.getFalsePositives() + this.nn2.getFalseNegatives();
			this.tp = this.nn1.getTruePositives() + this.nn2.getTruePositives();
			this.fp = this.nn1.getFalsePositives() + this.nn2.getFalsePositives();
			this.tn = this.nn1.getTrueNegatives() + this.nn2.getTrueNegatives();
			this.fn = this.nn1.getFalseNegatives() + this.nn2.getFalseNegatives();
			this.total = this.right + this.left;
			double dright = (double) this.right;
			double dleft = (double) this.left;
			this.ga = 100d * dright / (dright + dleft);
			double tpd = (double) this.tp;
			double fpd = (double) this.fp;
			double tnd = (double) this.tn;
			double fnd = (double) this.fn;
			this.sensitivity = 100d * tpd / (fpd + tpd);
			this.specificity = 100d * tnd / (fnd + tnd);
			this.minga = Math.min(this.nn1.getMinGa(), this.nn2.getMinGa());
			double d = (double) this.discrepancies;
			this.bias = 100d * (fpd + fnd) / (d + fpd + fnd);
		}
		this.ready = true;
		this.separator = null;
		this.predictors = null;
	}

	@Override
	public int getTotal() {
		return this.total;
	}

	@Override
	public int[] getPredictorsPower() {
		return this.nn3.getPredictorsPower();
	}

	@Override
	public void setQuality(double bias, double sensitivity, double specificity,
			double ga, int tp, int fp, int tn, int fn, int discrepancies) {
		this.bias = bias;
		this.sensitivity = sensitivity;
		this.specificity = specificity;
		this.ga = ga;
		this.tp = tp;
		this.fp = fp;
		this.tn = tn;
		this.fn = fn;
		this.discrepancies = discrepancies;

	}

}
