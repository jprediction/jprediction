/*
    libvmr
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package libvmr.classificators.ternary;

import java.io.*;

import libvmr.classificators.neuronets.*;
import libvmr.separators.*;

/**
 * ��������� ���������� ��� ����������
 * @author Yury V. Reshetov
 * @version 10.00
 */
public interface TernaryClassificator extends Serializable {

	/**
	 * �������� ���������
	 * @param pattern ������� ������ - �������
	 * @return ���������
	 */
	public String getResult(double[] pattern);
	
	/**
	 * �������� ������ ��������� ������
	 * @return ������ ��������� ������
	 */
	public NeuroNet getFirstBinaryClassificator();
	
	/**
	 * �������� ������ ��������� ������
	 * @return ������ ��������� ������
	 */
	public NeuroNet getSecondBinaryClassificator();
	
	/**
	 * �������� �������� �����������
	 * @return �������� �����������
	 */
	public String[] getIds();
	
	
	/**
	 * �������� ���������� ��� �����������
	 * @return the units
	 */
	public String[] getUnits();
	
	/**
	 * �������� �������
	 */
	public void train(Separator separator);

	/**
	 * �������� �������
	 */
	public void train(Separator separator, int[] predictors);
	
	/**
	 * ���������� ������� ����� ��������
	 * 
	 * @return ������, ���� ��� ������ �������
	 */
	public boolean isReady();
	
	/**
	 * ������ ���������� �����������
	 * @return ���������� �����������
	 */
	public int getDiscrepancies();
	
	/**
	 * ������ ���������� ��������� ������������������ ��������
	 * @return ���������� ��������� ������������������ ��������
	 */
	public int getRight();
	
	/**
	 * ������ ���������� ������� ������������������ ��������
	 * @return ���������� ������� ������������������ ��������
	 */
	public int getLeft();
	
	/**
	 * ������ ���������� ������� ������������� ��������
	 * @return ���������� ������� ������������� ��������
	 */
	public int getTp();
	
	/**
	 * ������ ���������� ����� ������������� ��������
	 * @return ���������� ����� ������������� ��������
	 */
	public int getFp();
	
	/**
	 * ������ ���������� ������� ������������� ��������
	 * @return ���������� ������� ������������� ��������
	 */
	public int getTn();
	
	/**
	 * ������ ���������� ����� ������������� ��������
	 * @return ���������� ����� ������������� ��������
	 */
	public int getFn();
	
	/**
	 * ������ ���������� �����������
	 * @return ���������� �����������
	 */
	public double getGa();
	
	/**
	 * �������� ����������� ���������� �����������
	 * @return ����������� ���������� �����������
	 */
	public double getMinGa();
	
	
	/**
	 * ������ ���������������� �������
	 * @return ���������������� �������
	 */
	public double getSensitivity();
	
	/**
	 * ������ ������������� �������
	 * @return ������������� �������
	 */
	public double getSpecificity();

	/**
	 * ������ ������������ �������
	 * @return ������������ �������
	 */
	public double getBias();
	
	/**
	 * ������, ������� �������� ���� � �������� �������
	 * @return ���������� �������� � �������� �������
	 */
	public int getTotal();
	
	/**
	 * �������� �������� ���������� �����������
	 * @return �������� ���������� �����������
	 */
	public int[] getPredictorsPower();
	
	/**
	 * ��������� ������������� ���������� �����������
	 * @param bias ������������
	 * @param sensitivity ����������������
	 * @param specificity �������������
	 * @param ga ���������� �����������
	 * @param tp ���������� ������� ������������� ��������
	 * @param fp ���������� ����� ������������� ��������
	 * @param tn ���������� ������� ������������� ��������
	 * @param fn ���������� ����� ������������� ��������
	 * @param discrepancies ���������� ������������
	 */
	public void setQuality(double bias, double sensitivity, double specificity,
			double ga, int tp, int fp, int tn, int fn, int discrepancies);
	
	
}
