/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package libvmr.classificators.neuronets.trainingalgorithms;

import java.util.*;

import libvmr.tools.*;

/**
 * �������� �������������� ������� �� ������ ������, ��������, ��������
 * 
 * @author Yury V. Reshetov
 * @version 10.00
 */
public class BrownRobinsonReshetovAlgorithm implements TrainingAlgorithm {

	private static final long serialVersionUID = 1000L;

	/**
	 * ������� ��� ������ �� ������� - ���������
	 */
	int[] counterrows = null;

	/**
	 * �� �������� �� ��������� ��������������?
	 */
	private boolean constantnonzero = false;

	/**
	 * ��������� ��������� �����
	 */
	private Random rand = new Random();
	/**
	 * ������� ������������ �������������� �������
	 */
	private double[] weights = null;
	
	/** 
	 * ���������� �������� �����������
	 */
	private int[] result = null;


	/**
	 * @return ������� ��� ������� ��������
	 */
	public int[] getCounterRows() {
		return counterrows;
	}

	/**
	 * @return �� �������� �� ��������� ��������������?
	 */
	@Override
	public boolean isConstantNonZero() {
		return constantnonzero;
	}
	
	
	@Override
	public double[] learning(double[][] samples, String[] variables,
			Accounting accounting) {
		double[][] doublesamples = this.doublling(samples);

		// ���������� �����
		int rows = doublesamples.length;
		// ���������� ��������
		int columns = doublesamples[0].length;
		counterrows = new int[rows];
		// ������������� ��������
		Arrays.fill(counterrows, 0);
		// ������� ��� ������ �� �������� - ��������� �������
		int[] countercolumns = new int[columns];
		// ������������� ��������
		Arrays.fill(countercolumns, 0);
		// ���������� ��� �����
		double[] rowsadder = new double[rows];
		Arrays.fill(rowsadder, 0d);
		// ���������� ��� ��������
		double[] collsadder = new double[columns];
		Arrays.fill(collsadder, 0d);
		// ������ ��������� ������
		int selectedrow = rand.nextInt(rows);
		// ������ ���������� �������
		int seletctedcolumn = 0;
		// ���������� �������� ��������
		int delta = doublesamples[0].length / 2;
		if (delta == 0) {
			delta = 1;
		}
		for (int u = 0; u < (doublesamples[0].length); u++) {
			// System.out.println(u);
			if (u == delta) {
				// ����� ���������� �������� �������� ������� ��������
				Arrays.fill(countercolumns, 0);
				Arrays.fill(this.counterrows, 0);
			}
			for (int t = 0; t < 10000; t++) {
				for (int j = 0; j < columns; j++) {
					collsadder[j] = collsadder[j]
							+ doublesamples[selectedrow][j];
				}
				seletctedcolumn = 0;
				// ���� ������ �������
				for (int j = 1; j < columns; j++) {
					if (collsadder[j] > collsadder[seletctedcolumn]) {
						seletctedcolumn = j;
					} else {
						if ((collsadder[j] == collsadder[seletctedcolumn])
								&& (countercolumns[j] < countercolumns[seletctedcolumn])) {
							seletctedcolumn = j;
						}
					}
				}
				// �������� ��������
				countercolumns[seletctedcolumn] = countercolumns[seletctedcolumn] + 1;
				// ���� ������, �������� ��������������� ��������
				for (int i = 0; i < rows; i++) {
					rowsadder[i] = rowsadder[i]
							+ doublesamples[i][seletctedcolumn];
				}
				selectedrow = 0;
				for (int i = 1; i < rows; i++) {
					if (rowsadder[i] < rowsadder[selectedrow]) {
						selectedrow = i;
					} else {
						if ((rowsadder[i] == rowsadder[selectedrow])
								&& (counterrows[i] < counterrows[selectedrow])) {
							selectedrow = i;
						}
					}
				}
				// ����������� ������� ��� ���������� �������
				counterrows[selectedrow] = counterrows[selectedrow] + 1;
			}
		}

		int count = columns / 2;
		// ���������� �������� ��� ��������
		this.result = new int[count];
		// ������� ������������ �������������� �������
		this.weights = new double[count];


		// ��������� �������� ��� ���������� ������� �������������
		double sum = 0d;
		for (int i = 0; i < count; i++) {
			// ��������� ������� �������� i-�� ������� ����� �� � ������
			this.result[i] = countercolumns[i] - countercolumns[i + count];
			// ����������� ������� i-�� �������� ������������ ��������������
			// �������
			this.weights[i] = this.result[i];
			sum = sum + this.weights[i];
		}

		this.constantnonzero = Math.abs(this.result[0]) > 0;


		for (int i = 0; i < count; i++) {
			// ��������� ������� ������������ �������������� �������
			this.weights[i] = this.weights[i] / Math.abs(sum);
			// ���������� ���������� � ��������� ����
			// ���� ������� ����������� �� �������
			if (Math.abs(this.result[i]) > 0d) {
				// �� ��������� ���
				variables[i] = this.weights[i] + variables[i];
			} else {
				// ��������� ������� ������������ ��� �� �����
				variables[i] = "";
				accounting.clearPredictor(i);
			}
		}

		return this.weights;
	}
	

	/**
	 * ����������� ����� �������
	 * 
	 * @param samples
	 *            ������� ������
	 * @return �������� ������, ������ ������ �������� ��������� ����� �� ����
	 *         ���������� ������������� �������� �� ���������
	 */
	private double[][] doublling(double[][] samples) {
		double[][] result = new double[samples.length][samples[0].length * 2];
		for (int i = 0; i < samples.length; i++) {
			for (int j = 0; j < samples[0].length; j++) {
				result[i][j] = samples[i][j];
				result[i][j + samples[0].length] = -samples[i][j];
			}
		}
		return result;
	}
	
	@Override
	public TrainingAlgorithm clone() {
		return new BrownRobinsonReshetovAlgorithm();
	}

	@Override
	public int[] getResults() {
		return this.result;
	}
	
}
