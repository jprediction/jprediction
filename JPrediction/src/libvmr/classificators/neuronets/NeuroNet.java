/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package libvmr.classificators.neuronets;

import java.io.*;

import libvmr.classificators.neuronets.hiddenlayers.HiddenLayer;
import libvmr.classificators.neuronets.trainingalgorithms.*;
import libvmr.normsetters.*;
import libvmr.parsers.Parser;

/**
 * ��������� ��������� ������ �������� - VMR
 * 
 * @author Yury V. Reshetov
 * @version 10.00
 */

public interface NeuroNet extends Serializable {
	
	/**
	 * ���������� ��������� ��������
	 * @param ���������
	 *            ������ ��� ���������� ���������
	 */
	public void setTrainer(TrainingAlgorithm trainer);
	
	/**
	 * ��������� ������������
	 * @param ���������
	 *            ������ ��� ������������
	 */
	public void setNormsetter(NormSetter normsetter);
	
	/**
	 * ��������� ���������� �����������
	 * @return ���������� ������ ��� ��������� �������
	 */
	public int getOutSampleErrors();
	
	/**
	 * ������ �������� ���������� ��������
	 * @return the indicator by Reshetov
	 */
	public double getIndicatorByReshetov();

	/**
	 * ������ ������� ����
	 * @param ������� ����
	 */
	public void setHiddenLayer(HiddenLayer hiddenlayer);
	
	/**
	 * ������ ���������� �����������
	 * @return ���������� ����������� � ���������
	 */
	public double getGeneralizationAbility();

	/**
	 * �������� ���������������� ���������� �����������
	 * @return ���������������� ���������� ����������� � ���������
	 */
	public double getSensitivity();
	
	/**
	 * ������ ������������� ���������� �����������
	 * @return ������������� ���������� ����������� � ���������
	 */
	public double getSpecificity();
	
	/**
	 * �������� ���������� �������������������� �������
	 * 
	 * @return ���������� �������������������� �������
	 */
	public int getTruePositives();
	
	/**
	 * �������� ���������� �������������������� �������
	 * 
	 * @return ���������� �������������������� �������
	 */
	public int getTrueNegatives();
	
	/**
	 * �������� ���������� ������������������� �������
	 * 
	 * @return ���������� ������������������ �������
	 */
	public int getFalsePositives();
	
	/**
	 * �������� ���������� �������������������� �������
	 * 
	 * @return ���������� �������������������� �������
	 */
	public int getFalseNegatives();
	
	/**
	 * �������� ��������� ���� �� ������ ��������
	 * 
	 * @param samples
	 *            ��������� �������
	 * @return true - ���������� ������ �������� ���������� ������������, false
	 *         - �� ��������
	 */
	public boolean train(double[][] trainsamples, double[][] fulltestsamples);
	
	/**
	 * ��������� ����������� ���������� �������������� �������
	 * 
	 * @param pattern
	 *            ��������������� ������� �������� ��� ������������ �������
	 * @return ���������
	 */
	public double getDecision(double[] pattern);
	
	/**
	 * �������� �������� ��� ���������� �������������� �� Java
	 * @return �������� ���
	 */
	public String getJavaCode();
	
	/**
	 * �������� ������
	 * @return ������
	 */
	public Parser getParser();

	/**
	 * �������� ������ ���������� �����������
	 * @return ������ ���������� �����������
	 */
	public int[] getPredictorsPower();
	
	/**
	 * ������ ����������� ���������� �����������
	 * @return ����������� ���������� �����������
	 */
	public double getMinGa();
	
}
