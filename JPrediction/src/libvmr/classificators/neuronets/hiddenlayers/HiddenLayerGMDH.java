/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package libvmr.classificators.neuronets.hiddenlayers;

import libvmr.classificators.neuronets.hiddenlayers.neurons.*;
import libvmr.tools.*;

import java.util.*;


/**
 * ������� ���� ��������� �� ������
 *  ���������� ����� ���������� �. �. ���������
 * 
 * @author Yury V. Reshetov
 * @version 8.04
 */
public class HiddenLayerGMDH implements HiddenLayer {

	private static final long serialVersionUID = 804L;
	/**
	 * �������������� ������������ ����������
	 */
	private String[] variables = null;
	/**
	 * ��������� ������ ��� ����� ������������ ����������
	 */
	private Accounting accounting = null;

	/**
	 * ������ ������������ ��������
	 */
	private ArrayList<Neuron> neurons = new ArrayList<Neuron>();
	

	
	

	/**
	 * ���������� �������������� ������������ ����������
	 * 
	 * @return �������������� ����������
	 */
	@Override
	public String[] getVariables() {
		return this.variables;
	}

	/**
	 * ������������� ��������� ������
	 * 
	 * @return ��������������� ������
	 */
	@Override
	public double[][] getTransformData(double[][] samples) {
		int count = 1;
		for (int i = 0; i < (samples[0].length); i++) {
			count = count * 2;
		}
		
		this.neurons.add(new ConstantNeuron(0));
		
		for (int i = 1; i < count; i++) {
			GMDHNeuron neuron = new GMDHNeuron(samples[0].length, i);
			this.neurons.add(neuron);
		}
		
		count = this.neurons.size();

		this.variables = new String[count];
		this.accounting = new Accounting(this.variables.length,
				samples[0].length);

		double[][] result = new double[samples.length][count];
		for (int i = 0; i < samples.length; i++) {
			for (int j = 0; j < count; j++) {
				Neuron neuron = this.neurons.get(j);
				this.variables[j] = " * " + neuron.toString();
				result[i][j] = neuron.getResult(samples[i]);
				for (int n = 0; n < samples[0].length; n++) {
					if (neuron.isWeigthNotNull(n)) {
						this.accounting.setValue(j, n);
					}
					
				}
			}
		}
		return result;
	}

	/**
	 * ������������� ���������� ������
	 * 
	 * @return
	 */
	@Override
	public double[] getTransformData(double[] sample) {
		int count = this.neurons.size();
		double[] result = new double[count];
		for (int i = 0; i < count; i++) {
			result[i] = this.neurons.get(i).getResult(sample);
		}

		return result;
	}

	/**
	 * ������� ��������� ������ ����� ������������ ����������
	 * 
	 * @return ��������� ������ ����� ������������ ����������
	 */
	@Override
	public Accounting getAccounting() {
		return this.accounting;
	}

	public HiddenLayer clone() {
		return new HiddenLayerPerceptron();
	}

	/**
	 * �������� ��� ������� ��������
	 * @return ��� �������
	 */
	@Override
	public String getSigmoidString() {
		return this.neurons.get(1).getSigmoidString();
	}

	
	@Override
	public ArrayList<Neuron> getNeurons() {
		return this.neurons;
	}
	
	@Override
	public double[] reduction(double[] weigths, int[] results) {
		ArrayList<Neuron> temp = new ArrayList<Neuron>();
		double[] tempweights = new double[weigths.length];
		temp.add(this.neurons.get(0));
		tempweights[0] = weigths[0];
		int n = 1;
		for (int i = 1; i < this.neurons.size(); i++) {
			if (results[i] != 0) {
				temp.add(this.neurons.get(i));
				tempweights[n] = weigths[i];
				n++;
			}
		}
		this.neurons = temp;
		double[] result = new double[this.neurons.size()]; 
		for (int i = 0; i < this.neurons.size(); i++) {
			result[i] = tempweights[i];
		}
		return result;
	}
}
