/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

package libvmr.classificators.neuronets.hiddenlayers.neurons;

import java.io.*;

import libvmr.classificators.neuronets.hiddenlayers.sigmoids.*;

/**
 * ��������� �������������� ������� 
 * @author Yury V. Reshetov
 * @version 8.02
 */

public interface Neuron extends Serializable {
	
	
	/**
	 * �������� ��������� �� ������ �������
	 * @param sample ������� �������
	 * @return ���������
	 */
	public double getResult(double[] sample);
	
	/**
	 * ������, ����������� �� � ������� ���� � �������� i
	 * @param i ����� ������������ �����
	 * @return ������, ���� �����������. � ��������� ������ - ����.
	 */
	public boolean isWeigthNotNull(int i);

	/**
	 * �������� ��� ������� ��������
	 * @return ��� �������
	 */
	public String getSigmoidString();
	
	/**
	 * �������� ������������� �������
	 * @return ������������� �������
	 */
	public Sigmoid getSigmoida();	
	
	/**
	 * �������� ����� ������� � ���������
	 * @return �����
	 */
	public int getNumber();
	
	/**
	 * ���������� ������� �����������
	 * @param i ����� �������� ������������
	 * @param value �������� �������� ������������
	 */
	public void setWeight(int i, double value);

}
