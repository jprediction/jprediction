/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


package libvmr.classificators.neuronets.hiddenlayers.neurons;

import libvmr.classificators.neuronets.hiddenlayers.sigmoids.*;

/**
 * ��������� ������
 * 
 * @author Yury V. Reshetov
 * @version 8.02
 */


public class ConstantNeuron implements Neuron {

	/**
	 * ����� ������
	 */
	private static final long serialVersionUID = 802L;
	
	
	/**
	 * ����� ������� � ���������
	 */
	private int number = 0;
	
	

	/**
	 * �����������
	 * @param numberweights ���������� ������ ��������
	 */
	public ConstantNeuron(int numberneuron) {
		this.number = numberneuron;
	}
	
	@Override
	public double getResult(double[] sample) {
		return 1d;
	}
	
	@Override
	public boolean isWeigthNotNull(int i) {
		return false;
	}
	
	@Override
	public void setWeight(int i, double value) {
	}
	
	@Override
	public String toString() {
		return ("");
	}

	@Override
	public String getSigmoidString() {
		return "";
	}

	@Override
	public Sigmoid getSigmoida() {
		return null;
	}
	
	@Override
	public int getNumber() {
		return number;
	}
	
}
