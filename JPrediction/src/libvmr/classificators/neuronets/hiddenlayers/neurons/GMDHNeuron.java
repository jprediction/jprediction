/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


package libvmr.classificators.neuronets.hiddenlayers.neurons;

import libvmr.classificators.neuronets.hiddenlayers.sigmoids.*;

/**
 * ������ � �������������� ����������
 * 
 * @author Yury V. Reshetov
 * @version 8.02
 */


public class GMDHNeuron implements Neuron {

	/**
	 * ����� ������
	 */
	private static final long serialVersionUID = 802L;
	

	/**
	 * ������� ������������ �������
	 */
	private double[] weigths = null;
	
	/**
	 * ����� ������� � ���������
	 */
	private int number = 0;
	
	

	/**
	 * �����������
	 * @param numberweights ���������� ������ ��������
	 */
	public GMDHNeuron(int numberweights, int numberneuron) {
		this.number = numberneuron;
		this.weigths = new double[numberweights];
		int z = numberneuron;
		for (int n = 0; n < numberweights; n++) {
			this.weigths[n] = (double) (z % 2);
			z = z / 2;
		}
	}
	
	@Override
	public double getResult(double[] sample) {
		if (this.number == 0) {
			return 1d;
		}
		double result = 1d;
		for (int i = 0; i < this.weigths.length; i++) {
			if (this.weigths[i] > 0.5d) {
				result = result * sample[i];
			}
		}
		return result;
	}
	
	@Override
	public boolean isWeigthNotNull(int i) {
		return (this.weigths[i] != 0d);
	}
	
	@Override
	public void setWeight(int i, double value) {
		this.weigths[i] = value;
	}
	
	@Override
	public String toString() {
		String result = "";
		for (int i = 0; i < this.weigths.length; i++) {
			if (this.weigths[i] > 0.5d) {
				if (result.equals("")) {
					result = result + "x" + i;
				} else {
					result = result + " * x" + i;
				}
			}
		}
		return (result + "\n");
	}

	@Override
	public String getSigmoidString() {
		return "";
	}

	@Override
	public Sigmoid getSigmoida() {
		return null;
	}
	
	@Override
	public int getNumber() {
		return number;
	}
	
}
