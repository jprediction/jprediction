/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


package libvmr.classificators.neuronets.hiddenlayers.neurons;

import libvmr.classificators.neuronets.hiddenlayers.sigmoids.*;

/**
 * ������� ������
 * 
 * @author Yury V. Reshetov
 * @version 8.02
 */


public class SimpleNeuron implements Neuron {

	/**
	 * ����� ������
	 */
	private static final long serialVersionUID = 802L;
	

	/**
	 * ��������
	 */
	private double bias = 0d;
	
	/**
	 * ����� ������� � ���������
	 */
	private int number = 0;


	/**
	 * ������� ������������ �������
	 */
	private double[] weigths = null;
	
	/**
	 * ������������� �������
	 */
	private Sigmoid sigmoida = null; 
	
	

	/**
	 * �����������
	 * @param numberweights ���������� ������ ��������
	 * @param sigmoid ������������� �������
	 */
	public SimpleNeuron(int numberweights, Sigmoid sigmoid, int numberneuron) {
		this.number = numberneuron;
		this.sigmoida = sigmoid;
		this.weigths = new double[numberweights];
		int z = numberneuron;
		for (int n = 0; n < numberweights; n++) {
			int w = z % 2;
			if (w == 1) {
			}
			this.weigths[n] = (double)w;
			z = z / 2;
		}
		this.bias = (double) (z % 2);
	}
	
	@Override
	public double getResult(double[] sample) {
		double result = bias;
		for (int i = 0; i < this.weigths.length; i++) {
			result = result + this.weigths[i] * sample[i];
		}
		return this.sigmoida.getResult(result);
	}
	
	@Override
	public boolean isWeigthNotNull(int i) {
		return (this.weigths[i] != 0d);
	}
	
	@Override
	public void setWeight(int i, double value) {
		this.weigths[i] = value;
	}
	
	@Override
	public String toString() {
		String result = this.sigmoida.getName() + "(";
		if (this.bias > 0.5d) {
			result = result + "1d";
		}
		for (int i = 0; i < this.weigths.length; i++) {
			if (this.weigths[i] != 0d) {
				if (this.weigths[i] == -1) {
					result = result + " - x" + i;
				} else {
					if (result.endsWith("(")) {
						result = result + "x" + i;
					} else {
						result = result + " + x" + i;
					}
				}
			}
		}
		result = result + ")\n";
		return result;
	}
	

	
	/**
	 * �������� ��� ������� ��������
	 * @return ��� �������
	 */
	@Override
	public String getSigmoidString() {
		return this.sigmoida.toString();
	}
	
	/**
	 * �������� ������������� �������
	 * @return the sigmoida
	 */
	@Override
	public Sigmoid getSigmoida() {
		return this.sigmoida;
	}

	@Override
	public int getNumber() {
		return this.number;
	}

}
