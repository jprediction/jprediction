/*
    libvmr
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package libvmr.classificators.neuronets.hiddenlayers.sigmoids;


/**
 * ������� �������
 * 
 * @author Yury V. Reshetov
 * @version 5.00
 */
public class SimpleSigmoid implements Sigmoid {
	
	/**
	 * ������������� ������
	 */
	private static final long serialVersionUID = 500L;
	/**
	 * ������������ �������
	 */
	private String name = "sigmoid";


	/**
	 * �������� �������� ��������
	 * @param x �������� ���������
	 * @return �������� ��������
	 */
	@Override
	public double getResult(double x) {
		if (Math.abs(x) < 1d) {
			return 2d * Math.signum(x) - x;
		}
		return Math.signum(x);
	}
	
	/**
	 * �������� ��� ������� ��������
	 * @return ��� �������
	 */
	@Override
	public String toString() {
		String result = "double sigmoid(double x) {\n";
		result += "   if (Math.abs(x) < 1d) {\n";
		result += "      return 2d * Math.signum(x) - x;\n";
		result += "   }\n";
		result += "   return Math.signum(x);\n";
		result += "}\n";
		return result;
	}
	
	/**
	 * �������� ��� ��������
	 * @return the name
	 */
	@Override
	public String getName() {
		return name;
	}

}
