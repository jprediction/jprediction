/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package libvmr.classificators.neuronets;

import java.util.ArrayList;
import java.util.Arrays;

import libvmr.classificators.neuronets.hiddenlayers.*;
import libvmr.classificators.neuronets.hiddenlayers.neurons.*;
import libvmr.classificators.neuronets.trainingalgorithms.*;
import libvmr.normsetters.*;
import libvmr.parsers.*;
import libvmr.tools.*;

/**
 * ����������� ��������� ������ �������� - VMR
 * 
 * @author Yury V. Reshetov
 * @version 10.00
 */
public class GMDHNeuroNet implements NeuroNet {
	
	/**
	 * ������ ��� ������������
	 */
	private static final long serialVersionUID = 1000L;
	

	/**
	 * ���������� �����������
	 */
	private int[] predictorspower = null;


	/**
	 * �����������
	 */
	private NormSetter normsetter = null;

	/**
	 * ��������� �������� ��� �������������� �������
	 */
	private TrainingAlgorithm trainingalgorithm = null;

	/**
	 * ������� ��������������
	 */
	private HiddenLayer hiddenlayer = null;
	/**
	 * ������� ������������ ��������� ����
	 */
	private double[] weights = null;
	/**
	 * ������� �������������������� ��������
	 */
	private int truepositive = 0;
	/**
	 * ������� �������������������� ��������
	 */
	private int truenegative = 0;
	/**
	 * ������� ������������������ ��������
	 */
	private int falsepositive = 0;
	/**
	 * ������� ������������������ ��������
	 */
	private int falsenegative = 0;
	/**
	 * ������������ ����������
	 */
	private String[] variables = null;
	/**
	 * ���������������� ���������� �����������
	 */
	private double se = 0d;

	/**
	 * ������������� ���������� �����������
	 */
	private double sp = 0d;

	/**
	 * ���������� ��������
	 */
	private double indicator = 0d;

	/**
	 * ���������� ������ ��� ��������� �������
	 */
	private int outsampleerrors = 0;

	/**
	 * ������
	 */
	private Parser parser = null;

	/**
	 * ����������� ��������� ������ ��������
	 * 
	 * @param parser
	 *            ������
	 */
	public GMDHNeuroNet(Parser parser) {
		this.parser = parser;
	}

	/**
	 * @param ���������
	 *            ������ ��� ���������� ���������
	 */
	@Override
	public void setTrainer(TrainingAlgorithm trainer) {
		this.trainingalgorithm = trainer;
	}

	/**
	 * @param ���������
	 *            ������ ��� ������������
	 */
	@Override
	public void setNormsetter(NormSetter normsetter) {
		this.normsetter = normsetter;
	}

	/**
	 * @return ���������� ������ ��� ��������� �������
	 */
	@Override
	public int getOutSampleErrors() {
		return outsampleerrors;
	}

	/**
	 * @return the indicator by Reshetov
	 */
	@Override
	public double getIndicatorByReshetov() {
		return indicator;
	}

	/**
	 * ���������� ����������� � ���������
	 */
	private double ga = 0d;
	
	/**
	 * ����������� ���������� ����������� � ���������
	 */
	private double minga = 0d;
	

	@Override
	public double getMinGa() {
		return this.minga;
	}

	@Override
	public void setHiddenLayer(HiddenLayer hiddenlayer) {
		this.hiddenlayer = hiddenlayer;
	}

	/**
	 * @return ���������� ����������� � ���������
	 */
	@Override
	public double getGeneralizationAbility() {
		return ga;
	}

	/**
	 * @return ���������������� ���������� ����������� � ���������
	 */
	@Override
	public double getSensitivity() {
		return se;
	}

	/**
	 * @return ������������� ���������� ����������� � ���������
	 */
	@Override
	public double getSpecificity() {
		return sp;
	}

	/**
	 * �������� ���������� �������������������� �������
	 * 
	 * @return ���������� �������������������� �������
	 */
	@Override
	public int getTruePositives() {
		return this.truepositive;
	}

	/**
	 * �������� ���������� �������������������� �������
	 * 
	 * @return ���������� �������������������� �������
	 */
	@Override
	public int getTrueNegatives() {
		return this.truenegative;
	}

	/**
	 * �������� ���������� ������������������� �������
	 * 
	 * @return ���������� ������������������ �������
	 */
	@Override
	public int getFalsePositives() {
		return this.falsepositive;
	}

	/**
	 * �������� ���������� �������������������� �������
	 * 
	 * @return ���������� �������������������� �������
	 */
	@Override
	public int getFalseNegatives() {
		return this.falsenegative;
	}

	/**
	 * �������� ��������� ���� �� ������ ��������
	 * 
	 * @param samples
	 *            ��������� �������
	 * @return true - ���������� ������ �������� ���������� ������������, false
	 *         - �� ��������
	 */
	@Override
	public boolean train(double[][] trainsamples,
			double[][] testsamples) {

		if (this.hiddenlayer == null) {
			if (trainsamples[0].length < 12) {
				this.hiddenlayer = new HiddenLayerGMDH();
			} else {
				return false;
			}
		}

		if (this.normsetter == null) {
			this.normsetter = new SimpleNormSetter();
		}

		double[][] invariantsamples = this.getInvariant(this.hiddenlayer
				.getTransformData(this.normsetter.normSetting(trainsamples)),
				this.normsetter);

		this.variables = this.hiddenlayer.getVariables();

		if (this.trainingalgorithm == null) {
			this.trainingalgorithm = new BrownRobinsonReshetovAlgorithm();
		}

		// �������� - ����� ���������
		this.weights = this.trainingalgorithm.learning(invariantsamples,
				this.variables, this.hiddenlayer.getAccounting());
		

		/*
		// ��������� ���������� �����������
		this.predictorspower = new int[trainsamples[0].length - 1];
		Arrays.fill(predictorspower, 0);
		ArrayList<Neuron> neurons = this.hiddenlayer.getNeurons();
		boolean[] usepredictors = new boolean[predictorspower.length];
		Arrays.fill(usepredictors, false);
		for (int n = 0; n < neurons.size(); n++) {
			Neuron neuron = neurons.get(n);
			for (int p = 0; p < usepredictors.length; p++) {
				if (neuron.isWeigthNotNull(p)) {
					usepredictors[p] = true;
				}
			}
		}
		for (int p = 0; p < usepredictors.length; p++) {
			if (usepredictors[p]) {
				predictorspower[p] = predictorspower[p] + 1;
			}
		}
		*/
		
		// ��������� ���������� �����������
		this.predictorspower = new int[trainsamples[0].length - 1];
		Arrays.fill(predictorspower, 0);
		int[] predictorscounter = this.trainingalgorithm.getResults();
		ArrayList<Neuron> neurons = this.hiddenlayer.getNeurons();
		for (int n = 0; n < neurons.size(); n++) {
			Neuron neuron = neurons.get(n);
			for (int p = 0; p < predictorspower.length; p++) {
				if (neuron.isWeigthNotNull(p)) {
					if (Math.abs(predictorscounter[n]) > this.predictorspower[p]) {
						this.predictorspower[p] = Math
								.abs(predictorscounter[n]);
					}
				}
			}
		}
		
		// �������� ������ �������� �� �������� ����
		this.weights = this.hiddenlayer.reduction(this.weights, this.trainingalgorithm.getResults());

		// ��������� ���������� ��������

		this.indicator = 0d;
		for (int i = 0; i < testsamples.length; i++) {
			double testresult = this.getDecision(testsamples[i]);
			double ideal = 2d * (testsamples[i][testsamples[0].length - 1]) - 1d;
			double scores = testresult * ideal;
			this.indicator = this.indicator + scores;
			if (scores < 0) {
				this.outsampleerrors++;
			}
		}

		{
			double weightssum = 0d;
			for (int i = 0; i < this.weights.length; i++) {
				weightssum = weightssum + Math.abs(this.weights[i]);
			}
			this.indicator = this.indicator / weightssum;
		}

		// ��������� �������������� ����������
		// double[][] testsamples = separator.getStatisticalTestSamples();
		for (int i = 0; i < testsamples.length; i++) {
			double testresult = this.getDecision(testsamples[i]);
			double ideal = 2d * (testsamples[i][testsamples[0].length - 1]) - 1d;
			// �������� �����
			if (testresult * ideal > 0) {
				if (ideal > 0) {
					// �������������� ���������� �������������������� ��������
					this.truepositive++;
				} else {
					// �������������� ���������� �������������������� ��������
					this.truenegative++;
				}
			} else {
				if (testresult > 0) {
					// �������������� ���������� ������������������ ��������
					this.falsepositive++;
				} else {
					// �������������� ���������� ������������������ ��������
					this.falsenegative++;
				}
			}
		}

		double tpd = (double) this.truepositive;
		double fpd = (double) this.falsepositive;
		double tnd = (double) this.truenegative;
		double fnd = (double) this.falsenegative;
		// ��������� ����������������
		this.se = tpd / (tpd + fpd);
		// ��������� �������������
		this.sp = tnd / (tnd + fnd);
		// ��������� ���������� �����������
		this.ga = (this.se + this.sp - 1d) * 100d;
		// ��������� ������� ������������� �������
		double fp = (tpd + fnd) / (tpd + fnd + tnd + fpd);
		// ��������� ������� ������������� �������
		double fn = (tnd + fpd) / (tpd + fnd + tnd + fpd);

		// �������� ����� �� ������� ������� ���������� �����������
		boolean output = ((this.se > fp) && (sp > fn));

		if ((this.ga < 0d) || (!output)) {
			output = false;
			this.ga = 0d;
		}
		this.se = this.se * 100d;
		this.sp = this.sp * 100d;

		this.minga = Math.min(this.se, this.sp);

		return output;
	}

	/**
	 * ��������� ����������� ���������� �������������� �������
	 * 
	 * @param pattern
	 *            ��������������� ������� �������� ��� ������������ �������
	 * @return ���������
	 */
	@Override
	public double getDecision(double[] pattern) {

		double[] sample = this.hiddenlayer.getTransformData(this.normsetter
				.normSetting(pattern));

		double result = 0d;
		for (int i = 0; i < sample.length; i++) {
			result = result + sample[i] * this.weights[i];
		}
		return result;
	}

	/**
	 * ������������ �������������� ������� � ���������� ���� ��� �������� ������
	 * ����� �������������� ���������� ����� ����� �������
	 * 
	 * @param sample
	 *            ������� ��� ����������
	 * @return ������������ �������
	 */
	private double[][] getInvariant(double[][] sample, NormSetter normsetter) {
		double[][] result = new double[sample.length][sample[0].length];
		double[] ideals = normsetter.getIdeals();
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[0].length; j++) {
				result[i][j] = sample[i][j] / ideals[i];
			}
		}
		return result;
	}

	@Override
	public String getJavaCode() {
		String result = "/**\n";

		result = result + " * The quality of modeling in out of sample:\n";
		result = result + " *\n";
		result = result + " * TruePositives: " + this.truepositive + "\n";
		result = result + " * TrueNegatives: " + this.truenegative + "\n";
		result = result + " * FalsePositives: " + this.falsepositive + "\n";
		result = result + " * FalseNegatives: " + this.falsenegative + "\n";
		int total = this.truepositive + this.truenegative + this.falsepositive
				+ this.falsenegative;
		result = result
				+ " * Total patterns in out of samples with statistics: "
				+ total + "\n";
		this.outsampleerrors = this.falsepositive + this.falsenegative;
		result = result + " * Total errors in out of sample: "
				+ this.outsampleerrors + "\n";
		result = result + " * Sensitivity of generalization abiliy: " + this.se
				+ "%\n";
		result = result + " * Specificity of generalization ability: "
				+ this.sp + "%\n";
		result = result + " * Generalization ability: " + this.ga + "%\n";
		// result = result + " * Indicator by Reshetov: " + this.indicator +
		// "\n";
		result = result + "*/\n";

		Accounting accounting = this.hiddenlayer.getAccounting();
		result += "double getBinaryClassificator2(";
		for (int i = 0; i < accounting.getPredictorsCount(); i++) {
			if (result.endsWith("(")) {
				result += "double v" + i;
			} else {
				result += ", double v" + i;
			}
		}
		result += ") {\n";
		String formula = "   double decision = ";

		if (this.trainingalgorithm.isConstantNonZero()) {
			formula = formula + this.weights[0] + "\n";
		}

		for (int i = 1; i < this.variables.length; i++) {
			if (!variables[i].equals("")) {
				if (variables[i].startsWith("-")) {
					formula = formula + "   " + variables[i];
				} else {
					formula = formula + "   + " + variables[i];
				}
			}
		}
		result = this.normsetter.NormSettingToString(accounting, result);
		result = result + formula + "   ;\nreturn decision;\n}\n";
		if (this.hiddenlayer.getSigmoidString() != null) {
			result += "\n" + this.hiddenlayer.getSigmoidString();
		}
		return result;
	}

	@Override
	public Parser getParser() {
		return this.parser;
	}

	@Override
	public int[] getPredictorsPower() {
		return this.predictorspower;
	}

}
