/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package libvmr.parsers;

import java.io.*;

/**
 * ������ �������� �����
 * 
 * @author Yury V. Reshetov
 * @version 5.00
 */
public interface Parser extends Serializable {
	
	/**
	 * �������
	 * 
	 * @param file
	 *            ��� �������� �����
	 * @return ������ ��������� �������
	 */
	public double[][] parsing(File file);
	
	/**
	 * ����� ���������� �������� ������� ����������
	 * 
	 * @return �������� ������� ����������
	 */
	public String[] getIDs();
	
	/**
	 * ����� ���������� �������� ������� ����������
	 * @param predictors ������� ������������ ����������
	 * @return �������� ������� ����������
	 */
	public String[] getIDs(int[] predictors);
	
	
	/**
	 * ����� ���������� ������������ ������ ���������
	 * 
	 * @return ������������ ������ ���������
	 */
	public String[] getUnits();
	
	/**
	 * ����� ���������� ������������ ������ ���������
	 * @param predictors ������� ������������ ������ ��������� 
	 * @return ������������ ������ ���������
	 */
	public String[] getUnits(int[] predictors);	
	
	/**
	 * ����� ���������� ��������� �� ������
	 * 
	 * @return ��������� �� ������, ���� null ���� ������ �����������
	 */
	public String getError();
	

}
