/*
    libVMR
    
    Copyright (C) 2014,  Yury V. Reshetov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

package libvmr.tools;

import java.util.*;

/**
 * ���������� ����������� ��������������� �������������������
 * @author Yury V. Reshetov
 * @version 10.00
 */

public class Randomizator {
	
	// ��������� ��������������� �������������������
	private static Random rand = new Random();
	
	/**
	 * ������������� ����� ���������� �������
	 * @param array �������� ��������� ������
	 * @return ������������ ��������� ������
	 */
	synchronized public static double[][] shuffleArray(double[][] array) {
		double[][] result = array.clone();
		double[] temp = null;
		int index = 0;
		int len = result.length;
		for (int i = 0; i < len; i++) {
			temp = result[i];
			do {
				index = rand.nextInt(len);
			} while (index == i);
			result[i] = result[index];
			result[index] = temp;
		}
		return result;
	}
}
